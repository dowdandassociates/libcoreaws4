/*
 *
 * libcoreaws4/src/coreaws4/Endpoint.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "Endpoint.hpp"

#include <sstream>
#include <utility>

namespace coreaws4
{

Endpoint::Endpoint()
{
}

Endpoint::Endpoint(const Endpoint& copy)
{
    scheme = copy.scheme;
    host = copy.host;
    port = copy.port;
}

Endpoint::Endpoint(Endpoint&& move)
{
    scheme = std::move(move.scheme);
    host = std::move(move.host);
    port = std::move(move.port);
}

Endpoint::~Endpoint()
{
}

Endpoint& Endpoint::operator=(const Endpoint& rhs)
{
    if (this == &rhs)
    {
        return *this;
    }

    scheme = rhs.scheme;
    host = rhs.host;
    port = rhs.port;

    return *this;
}

Endpoint& Endpoint::operator=(Endpoint&& rhs)
{
    scheme = std::move(rhs.scheme);
    host = std::move(rhs.host);
    port = std::move(rhs.port);

    return *this;
}

Endpoint::Endpoint(const Scheme& scheme,
                   const std::string& host,
                   const boost::optional<Port>& port) :
        scheme(scheme),
        host(host),
        port(port)
{
}

Endpoint::Endpoint(const Scheme& scheme,
                   const std::string& host) :
        scheme(scheme),
        host(host),
        port(boost::optional<Port>())
{
}

Endpoint::Endpoint(const Scheme& scheme,
                   const std::string& host,
                   Port port) :
        scheme(scheme),
        host(host),
        port(boost::optional<Port>())
{
}

bool Endpoint::operator==(const Endpoint& rhs) const
{
    return (scheme == rhs.scheme &&
            host == rhs.host &&
            port == rhs.port);
}

bool Endpoint::operator!=(const Endpoint& rhs) const
{
    return !((*this) == rhs);
}

bool Endpoint::isUsingDefaultPort() const
{
    return (!this->port ||
            (*(this->port) == 80 && this->scheme == Scheme::http) ||
            (*(this->port) == 443 && this->scheme == Scheme::https));
}

std::string Endpoint::getServer() const
{
    std::stringstream strbuf;

    strbuf << this->host;
    if (!isUsingDefaultPort())
    {
        strbuf << ':' << *(this->port);
    }

    return strbuf.str();
}

std::string Endpoint::toString() const
{
    std::stringstream strbuf;

    strbuf << show(this->scheme);
    strbuf << "://";
    strbuf << this->getServer();

    return strbuf.str();
}

Scheme Endpoint::getScheme() const
{
    return scheme;
}

void Endpoint::setScheme(const Scheme& scheme)
{
    this->scheme = scheme;
}

Endpoint& Endpoint::withScheme(const Scheme& scheme)
{
    setScheme(scheme);
    return *this;
}

std::string Endpoint::getHost() const
{
    return host;
}

void Endpoint::setHost(const std::string& host)
{
    this->host = host;
}

Endpoint& Endpoint::withHost(const std::string& host)
{
    setHost(host);
    return *this;
}

boost::optional<Port> Endpoint::getPort() const
{
    return port;
}

void Endpoint::setPort(boost::optional<Port> port)
{
    this->port = port;
}

void Endpoint::setPort(Port port)
{
    this->port = boost::optional<Port>(port);
}

Endpoint& Endpoint::withPort(const boost::optional<Port>& port)
{
    setPort(port);
    return *this;
}

Endpoint& Endpoint::withPort(Port port)
{
    setPort(port);
    return *this;
}

}

