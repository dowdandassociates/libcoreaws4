/*
 *
 * libcoreaws4/src/coreaws4/AWS3HttpsSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS3HttpsSigner.hpp"

#include <sstream>
#include <stdexcept>

#include <boost/optional.hpp>

#include "DateTimeUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws4
{

AWS3HttpsSigner::~AWS3HttpsSigner()
{
}

Request AWS3HttpsSigner::sign(const Credentials& credentials,
                              const Request& request,
                              std::time_t time)
{
    std::string algorithm = "HmacSHA256";

    HeaderMap headers = Signer::filterXAmznAuthorization(request.getHeaders());
    HeaderMap lowerHeaders = Signer::lowerCaseKey(headers);

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        boost::optional<std::string> value =
                boost::optional<std::string>(DateTimeUtils::formatRFC822(time));
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    auto sessionToken = credentials.getSessionToken();
    if (sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        boost::optional<std::string> value = sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = AWS3HttpsSigner::stringToSign(Request(request).
            withHeaders(headers));

    std::string signature = Signer::sign(algorithm,
                                         credentials.getSecretAccessKey(),
                                         stringToSign);

    std::stringstream xAmznAuthorization;
    xAmznAuthorization << "AWS3-HTTPS AWSAccessKeyId="
                       << credentials.getAccessKeyId()
                       << ",Algorithm="
                       << algorithm
                       << ",Signature="
                       << signature;

    headers.insert(HeaderEntry(
            "X-Amzn-Authorization",
            boost::optional<std::string>(xAmznAuthorization.str())));

    return Request(request).
            withHeaders(headers);
}

Request AWS3HttpsSigner::operator()(const Credentials& credentials,
                                    const Request& request,
                                    std::time_t time) const
{
    return AWS3HttpsSigner::sign(credentials, request, time);
}

std::string AWS3HttpsSigner::stringToSign(const Request& request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request.getHeaders()));

    HeaderMap::const_iterator ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end() && ptr->second)
    {
        return *(ptr->second);
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end() && ptr->second)
    {
        return *(ptr->second);
    }

    return std::string();
}

}

