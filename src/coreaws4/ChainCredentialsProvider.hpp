/*
 *
 * libcoreaws4/src/coreaws4/ChainCredentialsProvider.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__CHAIN_CREDENTIALS_PROVIDER
#define COREAWS4__CHAIN_CREDENTIALS_PROVIDER

#include "CredentialsProvider.hpp"

#include <list>

#include "CredentialsProviderHandle.hpp"

namespace coreaws4
{

class ChainCredentialsProvider : public CredentialsProvider
{
public:
    ChainCredentialsProvider() = delete;
    ChainCredentialsProvider(const ChainCredentialsProvider&) = delete;
    ChainCredentialsProvider(ChainCredentialsProvider&&) = delete;
    virtual ~ChainCredentialsProvider();
    ChainCredentialsProvider& operator=(const ChainCredentialsProvider&) = delete;
    ChainCredentialsProvider& operator=(ChainCredentialsProvider&&) = delete;

    ChainCredentialsProvider(
            const std::list<CredentialsProviderHandle> credentialsProviders);

    virtual boost::optional<Credentials> operator()();
    virtual boost::optional<Credentials> refresh();

private:
    std::list<CredentialsProviderHandle> credentialsProviders;
};

}

#endif // not COREAWS4__CHAIN_CREDENTIALS_PROVIDER

