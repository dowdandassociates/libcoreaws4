/*
 *
 * libcoreaws4/src/coreaws4/AWSHostNameUtils.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__AWS_HOST_NAME_UTILS
#define COREAWS4__AWS_HOST_NAME_UTILS

#include <string>

#include "Endpoint.hpp"

namespace coreaws4
{

class AWSHostNameUtils
{
public:
    AWSHostNameUtils() = delete;
    AWSHostNameUtils(const AWSHostNameUtils&) = delete;
    AWSHostNameUtils(AWSHostNameUtils&&) = delete;
    ~AWSHostNameUtils() = delete;
    AWSHostNameUtils& operator=(const AWSHostNameUtils&) = delete;
    AWSHostNameUtils& operator=(AWSHostNameUtils&&) = delete;

    static std::string parseRegionName(const Endpoint& endpoint);
    static std::string parseServiceName(const Endpoint& endpoint);
};

}

#endif // not COREAWS4__AWS_HOST_NAME_UTILS

