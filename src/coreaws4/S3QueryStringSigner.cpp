/*
 *
 * libcoreaws4/src/coreaws4/S3QueryStringSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "S3QueryStringSigner.hpp"

#include <sstream>
#include <stdexcept>

namespace coreaws4
{

S3QueryStringSigner::~S3QueryStringSigner()
{
}

Request S3QueryStringSigner::sign(const Credentials& credentials,
                                  const Request& request,
                                  std::time_t expires)
{
    if (credentials.getSessionToken())
    {
        throw std::invalid_argument(
                "Cannot sign S3 Query String with Session Credentials");
    }

    ParameterMap parameters(Signer::filterSignature(request.getParameters()));
    HeaderMap headers(Signer::filterAuthorization(request.getHeaders()));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    parameters.insert(ParameterEntry(
            "AWSAccessKeyId",
            boost::optional<std::string>(credentials.getAccessKeyId())));
    std::stringstream expiresStrbuf;
    expiresStrbuf << expires;
    std::string expiresStr = expiresStrbuf.str();
    parameters.insert(ParameterEntry("Expires",
                                     boost::optional<std::string>(expiresStr)));
    lowerHeaders.insert(HeaderEntry("date",
                                    boost::optional<std::string>(expiresStr)));

    std::string stringToSign = S3Signer::stringToSign(Request(request).
            withParameters(parameters).
            withHeaders(lowerHeaders));

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials.getSecretAccessKey(),
                                         stringToSign);

    parameters.insert(ParameterEntry("Signature",
                                     boost::optional<std::string>(signature)));

    return Request(request).
            withParameters(parameters).
            withHeaders(headers);
}

Request S3QueryStringSigner::operator()(const Credentials& credentials,
                                        const Request& request,
                                        std::time_t expires) const
{
    return S3QueryStringSigner::sign(credentials, request, expires);
}

}

