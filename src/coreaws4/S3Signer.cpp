/*
 *
 * libcoreaws4/src/coreaws4/S3Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "S3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include <boost/optional.hpp>

#include "DateTimeUtils.hpp"
#include "HttpUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws4
{

S3Signer::~S3Signer()
{
}

Request S3Signer::sign(const Credentials& credentials,
                       const Request& request,
                       std::time_t time)
{
     
    HeaderMap headers = Signer::filterAuthorization(request.getHeaders());
    HeaderMap lowerHeaders = Signer::lowerCaseKey(headers);

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        boost::optional<std::string> value =
                boost::optional<std::string>(DateTimeUtils::formatRFC822(time));

        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    auto sessionToken = credentials.getSessionToken();
    if (sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        boost::optional<std::string> value = sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = S3Signer::stringToSign(Request(request).
            withHeaders(headers));

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials.getSecretAccessKey(),
                                         stringToSign);

    std::stringstream authorizationValue;
    authorizationValue << "AWS " 
                       << credentials.getAccessKeyId()
                       << ':'
                       << signature;

    headers.insert(HeaderEntry(
            "Authorization",
            boost::optional<std::string>(authorizationValue.str())));

    return Request(request).
            withHeaders(headers);
}

Request S3Signer::operator()(const Credentials& credentials,
                             const Request& request,
                             std::time_t time) const
{
    return S3Signer::sign(credentials, request, time);
}

std::string S3Signer::stringToSign(const Request& request)
{
    HeaderMap lowerHeaders = Signer::lowerCaseKey(request.getHeaders());
    
    HeaderMap interestingHeaders;
    std::copy_if(
            lowerHeaders.begin(),
            lowerHeaders.end(),
            std::inserter(interestingHeaders,
                          interestingHeaders.end()),
            [] (const HeaderEntry& header) {
                return (header.first == "content-type" ||
                        header.first == "content-md5" ||
                        StringUtils::startsWith(header.first, "x-amz-"));
            });

    HeaderMap::const_iterator ptr = lowerHeaders.find("date");
    boost::optional<std::string> date = (ptr != lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end()) ?
            ptr->second : boost::optional<std::string>("");
    interestingHeaders.insert(HeaderEntry("date", date));

    if (interestingHeaders.find("content-type") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderEntry(
                "content-type",
                boost::optional<std::string>("")));
    }

    if (interestingHeaders.find("content-md5") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderEntry(
                "content-md5",
                boost::optional<std::string>("")));
    }

    auto requestParameters = request.getParameters();
    ParameterMap interestingParameters;
    std::copy_if(
            requestParameters.begin(),
            requestParameters.end(),
            std::inserter(interestingParameters,
                          interestingParameters.end()),
            [] (const ParameterEntry& header) {
                return (StringUtils::startsWith(header.first, "x-amz-"));
            });


    HeaderMap headers = std::accumulate(
            interestingParameters.begin(),
            interestingParameters.end(),
            interestingHeaders,
            [] (const HeaderMap& acc,const ParameterEntry& parameter) {
                HeaderMap headers = acc;
                std::string value = (parameter.second) ? *(parameter.second) : "";
                headers.insert(HeaderEntry(
                            parameter.first,
                            boost::optional<std::string>(value)));
                return headers;
            });

    std::string headersToSign = std::accumulate(
            headers.begin(),
            headers.end(),
            std::string(),
            [] (const std::string& acc, const HeaderEntry& header) {
                std::stringstream strbuf;

                std::string value = (header.second) ? *(header.second) : "";

                strbuf << acc;
                if (StringUtils::startsWith(header.first, "x-amz-"))
                {
                    strbuf << header.first << ':' << value;
                }
                else
                {
                    strbuf << value;
                }

                strbuf << '\n';

                return strbuf.str();
            });


    ParameterMap parameters;
    std::copy_if(
            requestParameters.begin(),
            requestParameters.end(),
            std::inserter(parameters, parameters.end()),
            [] (const ParameterEntry& parameter) {
                return (parameter.first == "acl" ||
                        parameter.first == "cors" ||
                        parameter.first == "delete" ||
                        parameter.first == "lifecycle" ||
                        parameter.first == "location" ||
                        parameter.first == "logging" ||
                        parameter.first == "notification" ||
                        parameter.first == "partNumber" ||
                        parameter.first == "policy" ||
                        parameter.first == "requestPayment" ||
                        parameter.first == "response-cache-control" ||
                        parameter.first == "response-content-disposition" ||
                        parameter.first == "response-content-encoding" ||
                        parameter.first == "response-content-language" ||
                        parameter.first == "response-content-type" ||
                        parameter.first == "response-expires" ||
                        parameter.first == "tagging" ||
                        parameter.first == "torrent" ||
                        parameter.first == "uploadId" ||
                        parameter.first == "uploads" ||
                        parameter.first == "versionId" ||
                        parameter.first == "versioning" ||
                        parameter.first == "versions" ||
                        parameter.first == "website");
            });

    std::string queryString = Signer::constructQueryString(
            parameters,
            [] (const std::string& acc, const ParameterEntry& parameter) {
                std::stringstream strbuf;
                strbuf << '&';
                strbuf << parameter.first;
                if (parameter.second)
                {
                    strbuf << '=' << *(parameter.second);
                }
                return acc + strbuf.str();
            });

    std::stringstream strbuf;
    strbuf << show(request.getHttpMethod()) << '\n';
    strbuf << headersToSign;

    if (StringUtils::isEmpty(request.getResourcePath()))
    {
        strbuf << '/';
    }
    else
    {
        strbuf << HttpUtils::urlEncodePath(request.getResourcePath());
    }

    if (!StringUtils::isEmpty(queryString))
    {
        strbuf << '?' << queryString;
    }

    return strbuf.str();
}

}

