/*
 *
 * libcoreaws4/src/coreaws4/CredentialsProvider.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__CREDENTIALS_PROVIDER
#define COREAWS4__CREDENTIALS_PROVIDER

#include <boost/optional.hpp>

#include "Credentials.hpp"

namespace coreaws4
{

class CredentialsProvider
{
public:
    CredentialsProvider() = default;
    CredentialsProvider(const CredentialsProvider&) = delete;
    CredentialsProvider(CredentialsProvider&&) = delete;
    virtual ~CredentialsProvider();
    CredentialsProvider& operator=(const CredentialsProvider&) = delete;
    CredentialsProvider& operator=(CredentialsProvider&&) = delete;

    virtual boost::optional<Credentials> operator()() = 0;
    virtual boost::optional<Credentials> refresh() = 0;
};

}

#endif // not COREAWS4__CREDENTIALS_PROVIDER

