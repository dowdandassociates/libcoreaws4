/*
 *
 * libcoreaws4/src/coreaws4/S3QueryStringSigner.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__S3_QUERY_STRING_SIGNER
#define COREAWS4__S3_QUERY_STRING_SIGNER

#include "S3Signer.hpp"

namespace coreaws4
{

class S3QueryStringSigner : public S3Signer
{
public:
    S3QueryStringSigner() = default;
    S3QueryStringSigner(const S3QueryStringSigner&) = delete;
    S3QueryStringSigner(S3QueryStringSigner&&) = delete;
    virtual ~S3QueryStringSigner();
    S3QueryStringSigner& operator=(const S3QueryStringSigner&) = delete;
    S3QueryStringSigner& operator=(S3QueryStringSigner&&) = delete;

    static Request sign(const Credentials& credentials,
                        const Request& request,
                        std::time_t expires);

    virtual Request operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t expires) const;
};

}

#endif // not COREAWS4__S3_QUERY_STRING_SIGNER

