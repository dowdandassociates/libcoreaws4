/*
 *
 * libcoreaws4/src/coreaws4/AWS2Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__AWS_2_SIGNER
#define COREAWS4__AWS_2_SIGNER

#include "Signer.hpp"

namespace coreaws4
{

class AWS2Signer : public Signer
{
public:
    AWS2Signer() = default;
    AWS2Signer(const AWS2Signer&) = delete;
    AWS2Signer(AWS2Signer&&) = delete;
    virtual ~AWS2Signer();
    AWS2Signer& operator=(const AWS2Signer&) = delete;
    AWS2Signer& operator=(AWS2Signer&&) = delete;

    static Request sign(const Credentials& credentials,
                        const Request& request,
                        std::time_t time);

    virtual Request operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const;

private:
    static std::string stringToSign(const Request& request);
};

}

#endif // not COREAWS4__AWS_2_SIGNER

