/*
 *
 * libcoreaws4/src/coreaws4/Scheme.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__SCHEME
#define COREAWS4__SCHEME

#include <string>

namespace coreaws4
{

enum class Scheme
{
    http,
    https
};

std::string show(Scheme scheme);

}

#endif // not COREAWS4__SCHEME

