/*
 *
 * libcoreaws4/src/coreaws4/CloudFrontSigner.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "CloudFrontSigner.hpp"

#include <sstream>

#include "DateTimeUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws4
{

CloudFrontSigner::~CloudFrontSigner()
{
}

Request CloudFrontSigner::sign(const Credentials& credentials,
                               const Request& request,
                               std::time_t time)
{
    HeaderMap headers(Signer::filterAuthorization(request.getHeaders()));
    HeaderMap lowerHeaders(Signer::lowerCaseKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() &&
            lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        boost::optional<std::string> value =
                boost::optional<std::string>(DateTimeUtils::formatRFC822(time));
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    auto sessionToken = credentials.getSessionToken();
    if (sessionToken &&
            lowerHeaders.find("x-amz-security-token") == lowerHeaders.end())
    {
        std::string key = "x-amz-security-token";
        boost::optional<std::string> value = sessionToken;
        headers.insert(HeaderEntry(key, value));
        lowerHeaders.insert(HeaderEntry(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = CloudFrontSigner::stringToSign(Request(request).
            withHeaders(headers));

    std::string signature = Signer::sign("HmacSHA1",
                                         credentials.getSecretAccessKey(),
                                         stringToSign);

    std::stringstream authorizationValue;
    authorizationValue << "AWS "
                       << credentials.getAccessKeyId()
                       << ':'
                       << signature;

    headers.insert(HeaderEntry(
            "Authorization",
            boost::optional<std::string>(authorizationValue.str())));

    return Request(request).
            withHeaders(headers);
}

Request CloudFrontSigner::operator()(const Credentials& credentials,
                                     const Request& request,
                                     std::time_t time) const
{
    return CloudFrontSigner::sign(credentials, request, time);
}

std::string CloudFrontSigner::stringToSign(const Request& request)
{
    HeaderMap lowerHeaders(Signer::lowerCaseKey(request.getHeaders()));

    HeaderMap::const_iterator ptr;

    ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end())
    {
        boost::optional<std::string> maybeString = ptr->second;
        if (maybeString)
        {
            return *maybeString;
        }
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end())
    {
        boost::optional<std::string> maybeString = ptr->second;
        if (maybeString)
        {
            return *maybeString;
        }
    }

    return std::string();
}

}

