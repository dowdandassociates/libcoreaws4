/*
 *
 * libcoreaws4/src/coreaws4/HttpRequest.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__HTTP_REQUEST
#define COREAWS4__HTTP_REQUEST

#include <map>
#include <string>

#include "HttpMethod.hpp"
#include "InputStreamHandle.hpp"
#include "Request.hpp"

namespace coreaws4
{

class HttpRequest
{
public:
    static const bool VERBOSE = false;
    static const long TIMEOUT = 0;

    HttpRequest();
    HttpRequest(const HttpRequest&) = default;
    HttpRequest(HttpRequest&&) = default;
    ~HttpRequest() = default;
    HttpRequest& operator=(const HttpRequest&) = default;
    HttpRequest& operator=(HttpRequest&&) = default;

    HttpRequest(const HttpMethod& method,
                const std::string& url,
                const std::map<std::string, std::string>& headers,
                InputStreamHandle content);

    HttpRequest(const HttpMethod& method,
                const std::string& url,
                const std::map<std::string, std::string>& headers,
                InputStreamHandle content,
                long timeout,
                bool verbose);

    HttpRequest(const Request& request);

    HttpRequest(const Request& request,
                long timeout,
                bool verbose);

    bool operator==(const HttpRequest& rhs) const;
    bool operator!=(const HttpRequest& rhs) const;

    HttpMethod getMethod() const;
    void setMethod(const HttpMethod& method);
    HttpRequest& withMethod(const HttpMethod& method);

    std::string getUrl() const;
    void setUrl(const std::string& url);
    HttpRequest& withUrl(const std::string& url);

    std::map<std::string, std::string> getHeaders() const;
    void setHeaders(const std::map<std::string, std::string>& headers);
    HttpRequest& withHeaders(const std::map<std::string, std::string>& headers);

    InputStreamHandle getContent() const;
    void setContent(InputStreamHandle content);
    HttpRequest& withContent(InputStreamHandle content);

    long getTimeout() const;
    void setTimeout(long timeout);
    HttpRequest& withTimeout(long timeout);

    bool getVerbose() const;
    void setVerbose(bool verbose);
    HttpRequest& withVerbose(bool verbose);

private:
    HttpMethod method;
    std::string url;
    std::map<std::string, std::string> headers;
    InputStreamHandle content;
    long timeout;
    bool verbose;

    static HttpRequest convertRequest(const Request& request,
                                      long timeout,
                                      bool verbose);
    static std::size_t getContentSize(const Request& request);
    static std::map<std::string, std::string> processHeaders(
            const HeaderMap& headers);
};

}

#endif // not COREAWS4__HTTP_REQUEST

