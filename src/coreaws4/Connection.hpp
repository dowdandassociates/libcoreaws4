/*
 *
 * libcoreaws4/src/coreaws4/Connection.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__CONNECTION
#define COREAWS4__CONNECTION

#include <map>
#include <string>

#include <curl/curl.h>

#include "HttpRequest.hpp"
#include "InputStreamHandle.hpp"
#include "OutputStreamHandle.hpp"
#include "Request.hpp"
#include "ResponseHandle.hpp"

namespace coreaws4
{

class Connection
{
public:
    Connection() = delete;
    Connection(const Connection&) = delete;
    Connection(Connection&&) = delete;
    ~Connection() = delete;
    Connection& operator=(const Connection&) = delete;
    Connection& operator=(Connection&&) = delete;

    static void init();
    static void cleanup();

    static size_t read(void* ptr, size_t size, size_t nmemb, void* data);
    static size_t write(void* ptr, size_t size, size_t nmemb, void* data);

    static ResponseHandle execute(const HttpRequest& request,
                                  OutputStreamHandle output,
                                  OutputStreamHandle header);
    static ResponseHandle execute(const HttpRequest& request,
                                  OutputStreamHandle output);
    static ResponseHandle execute(const HttpRequest& request);

    static ResponseHandle execute(const Request& request,
                                  OutputStreamHandle output,
                                  OutputStreamHandle header);
    static ResponseHandle execute(const Request& request,
                                  OutputStreamHandle output);
    static ResponseHandle execute(const Request& request);

private:
    static CURL* initCurl();
    static char* setErrorBuffer(CURL* curl);
    static void setFollowLocation(CURL* curl, bool follow);
    static curl_slist* setHttpHeaders(
            CURL* curl,
            const std::map<std::string, std::string>& headers);
    static curl_slist* setHttpHeader(
            curl_slist* acc,
            const std::pair<const std::string, std::string>& header);
    static void setInFileSize(CURL* curl, curl_off_t size);
    static void setInputStream(CURL* curl, InputStreamHandle inputStream);
    static void setOutputStream(CURL* curl, OutputStreamHandle outputStream);
    static void setHeaderStream(CURL* curl, OutputStreamHandle headerStream);
    static void setMethod(CURL* curl, HttpMethod method);
    static void setPostFields(CURL* curl, const std::string& postField);
    static void setPostFieldSize(CURL* curl, curl_off_t size);
    static void setURL(CURL* curl, const std::string& url);
    static void setVerbose(CURL* curl, bool verbose);
    static void setTimeout(CURL* curl, long timeout);
    static std::string error(CURLcode rc);
    static std::string error(const std::string& message, CURLcode rc);
};

}

#endif // not COREAWS4__CONNECTION

