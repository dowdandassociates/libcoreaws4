/*
 *
 * libcoreaws4/src/coreaws4/AWS2Signer.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "AWS2Signer.hpp"

#include <sstream>
#include <stdexcept>

#include <boost/optional.hpp>

#include "DateTimeUtils.hpp"

#include <iostream>

namespace coreaws4
{

AWS2Signer::~AWS2Signer()
{
}

Request AWS2Signer::sign(const Credentials& credentials,
                         const Request& request,
                         std::time_t time)
{
    auto parameters = Signer::filterSignature(request.getParameters());

    auto key = "AWSAccessKeyId";
    auto ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                credentials.getAccessKeyId() != *(ptr->second))
        {
            throw std::invalid_argument(
                    "AWSAccessKeyId does not match credentials");
        }
    }
    else
    {
        parameters.insert(ParameterEntry(
                key, boost::optional<std::string>(credentials.getAccessKeyId())));
    }

    key = "SecurityToken";
    ptr = parameters.find(key);
    auto sessionToken = credentials.getSessionToken();
    if (ptr != parameters.end())
    {
        if ((ptr->second &&
                sessionToken &&
                *(sessionToken) != *(ptr->second)) ||
                (ptr->second && !sessionToken) ||
                (!ptr->second && sessionToken))
        {
            throw std::invalid_argument(
                    "SecurityToken does not match credentials");
        }
    }
    else
    {
        if (sessionToken)
        {
            parameters.insert(ParameterEntry(
                    key,
                    sessionToken));
        }
    }

    std::string algorithm;
    key = "SignatureMethod";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                (*(ptr->second) != "HmacSHA1" &&
                *(ptr->second) != "HmacSHA256"))
        {
            throw std::invalid_argument("Invalid SignatureMethod");
        }
        algorithm = *(ptr->second);
    }
    else
    {
        algorithm = "HmacSHA256";
        parameters.insert(ParameterEntry(
                key, boost::optional<std::string>(algorithm)));
    }

    key = "SignatureVersion";
    ptr = parameters.find(key);
    if (ptr != parameters.end())
    {
        if (!ptr->second ||
                *(ptr->second) != "2")
        {
            throw std::invalid_argument("Invalid SignatureVersion");
        }
    }
    else
    {
        parameters.insert(ParameterEntry(
                key, boost::optional<std::string>("2")));
    }

    ptr = parameters.find("Expires");
    if (ptr == parameters.end())
    {
        ptr = parameters.find("Timestamp");
        if (ptr == parameters.end())
        {
            parameters.insert(ParameterEntry(
                    "Timestamp", 
                    boost::optional<std::string>(
                            DateTimeUtils::formatISO8601(time))));
        }
    }

    auto stringToSign = AWS2Signer::stringToSign(Request(request).withParameters(parameters));

    auto signature = Signer::sign(algorithm,
                                  credentials.getSecretAccessKey(),
                                  stringToSign);
    parameters.insert(ParameterEntry(
            "Signature", boost::optional<std::string>(signature)));

    return Request(request).withParameters(parameters);
}

Request AWS2Signer::operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const
{
    return AWS2Signer::sign(credentials, request, time);
}

std::string AWS2Signer::stringToSign(const Request& request)
{
    std::stringstream strbuf;

    strbuf << show(request.getHttpMethod()) << '\n'
           << Signer::canonicalizedEndpoint(request.getEndpoint()) << '\n'
           << Signer::canonicalizedResourcePath(request.getResourcePath()) << '\n'
           << Signer::canonicalizedQueryString(request.getParameters());

    return strbuf.str();
}

}

