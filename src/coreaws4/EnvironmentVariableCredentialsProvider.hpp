/*
 *
 * libcoreaws4/src/coreaws4/EnvironmentVariableCredentialsProvider.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__ENVIRONMENT_VARIABLE_CREDENTIALS_PROVIDER
#define COREAWS4__ENVIRONMENT_VARIABLE_CREDENTIALS_PROVIDER

#include "CredentialsProvider.hpp"

namespace coreaws4
{

class EnvironmentVariableCredentialsProvider : public CredentialsProvider
{
public:
    EnvironmentVariableCredentialsProvider() = default;
    EnvironmentVariableCredentialsProvider(const EnvironmentVariableCredentialsProvider&) = delete;
    EnvironmentVariableCredentialsProvider(EnvironmentVariableCredentialsProvider&&) = delete;
    virtual ~EnvironmentVariableCredentialsProvider();
    EnvironmentVariableCredentialsProvider& operator=(const EnvironmentVariableCredentialsProvider&) = delete;
    EnvironmentVariableCredentialsProvider& operator=(EnvironmentVariableCredentialsProvider&&) = delete;

    virtual boost::optional<Credentials> operator()();
    virtual boost::optional<Credentials> refresh();
};

}

#endif // not COREAWS4__ENVIRONMENT_VARIABLE_CREDENTIALS_PROVIDER

