/*
 *
 * libcoreaws4/src/coreaws4/Request.hpp

 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__REQUEST
#define COREAWS4__REQUEST

#include <string>

#include <boost/optional.hpp>

#include "Endpoint.hpp"
#include "HttpMethod.hpp"
#include "InputStreamHandle.hpp"
#include "ParameterMap.hpp"
#include "HeaderMap.hpp"

namespace coreaws4
{

class Request
{
public:
    Request();
    Request(const Request& copy);
    Request(Request&& move);
    ~Request();
    Request& operator=(const Request& rhs);
    Request& operator=(Request&& rhs);

    Request(const HttpMethod& httpMethod,
            const Endpoint& endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers,
            const boost::optional<InputStreamHandle>& content);

    Request(const HttpMethod& httpMethod,
            const Endpoint& endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers,
            InputStreamHandle content);

    Request(const HttpMethod& httpMethod,
            const Endpoint& endpoint,
            const std::string& resourcePath,
            const ParameterMap& parameters,
            const HeaderMap& headers);

    bool operator==(const Request& rhs) const;
    bool operator!=(const Request& rhs) const;

    HttpMethod getHttpMethod() const;
    void setHttpMethod(const HttpMethod& httpMethod);
    Request& withHttpMethod(const HttpMethod& httpMethod);

    Endpoint getEndpoint() const;
    void setEndpoint(const Endpoint& endpoint);
    Request& withEndpoint(const Endpoint& endpoint);

    std::string getResourcePath() const;
    void setResourcePath(const std::string& resourcePath);
    Request& withResourcePath(const std::string& resourcePath);

    ParameterMap getParameters() const;
    void setParameters(const ParameterMap& parameters);
    Request& withParameters(const ParameterMap& parameters);

    HeaderMap getHeaders() const;
    void setHeaders(const HeaderMap& headers);
    Request& withHeaders(const HeaderMap& headers);

    boost::optional<InputStreamHandle> getContent() const;
    void setContent(const boost::optional<InputStreamHandle>& content);
    void setContent(InputStreamHandle content);
    Request& withContent(const boost::optional<InputStreamHandle>& content);
    Request& withContent(InputStreamHandle content);

private:
    HttpMethod httpMethod;
    Endpoint endpoint;
    std::string resourcePath;
    ParameterMap parameters;
    HeaderMap headers;
    boost::optional<InputStreamHandle> content;
};

}

#endif // not COREAWS4__REQUEST

