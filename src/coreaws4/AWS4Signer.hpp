/*
 *
 * libcoreaws4/src/coreaws4/AWS4Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__AWS_4_SIGNER
#define COREAWS4__AWS_4_SIGNER

#include "Buffer.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "Signer.hpp"

namespace coreaws4
{

class AWS4Signer : public Signer
{
public:
    AWS4Signer() = default;
    AWS4Signer(const AWS4Signer&) = delete;
    AWS4Signer(AWS4Signer&&) = delete;
    virtual ~AWS4Signer();
    AWS4Signer& operator=(const AWS4Signer&) = delete;
    AWS4Signer& operator=(AWS4Signer&&) = delete;

    static Request sign(const Credentials& credentials,
                        const Request& request,
                        std::time_t time);

    virtual Request operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const;

private:
    static std::string canonicalRequest(const Request& request);
    static std::string canonicalHeaders(const HeaderMap& lowerHeaders);
    static std::string buildCanonicalHeaders(const std::string& acc,
                                             const HeaderEntry& header);
    static std::string signedHeaders(const HeaderMap& lowerHeaders);
    static std::string buildSignedHeaders(const std::string& acc,
                                          const HeaderEntry& header);
    static std::string payloadHash(InputStreamHandle payload);
    static std::string scope(const std::string& dateStamp,
                             const std::string& region,
                             const std::string& service,
                             const std::string& terminator);
    static std::string signingCredentials(const Credentials& credentials,
                                          const std::string& scope);
    static std::string stringToSign(const std::string& algorithm,
                                    const std::string& iso8601,
                                    const std::string& scope,
                                    const std::string& canonicalRequest);
    static Buffer signature(const Credentials& credentials,
                            const std::string& dateStamp,
                            const std::string& region,
                            const std::string& service,
                            const std::string& terminator,
                            const std::string& stringToSign);
    static Buffer kSigning(const Credentials& credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service,
                           const std::string& terminator);
    static Buffer kService(const Credentials& credentials,
                           const std::string& dateStamp,
                           const std::string& region,
                           const std::string& service);
    static Buffer kRegion(const Credentials& credentials,
                          const std::string& dateStamp,
                          const std::string& region);
    static Buffer kDate(const Credentials& credentials,
                        const std::string& dateStamp);
    static Buffer kSecret(const Credentials& credentials);
    static std::string authorizationHeaderValue(
            const std::string& algorithm,
            const std::string& signingCredentials,
            const std::string& signedHeaders,
            const Buffer& signature);
};

}

#endif // not COREAWS4__AWS_4_SIGNER

