/*
 *
 * libcoreaws4/src/coreaws4/HttpRequest.cpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#include "HttpRequest.hpp"

#include <sstream>

#include "HeaderEntry.hpp"
#include "HttpUtils.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws4
{

HttpRequest::HttpRequest() :
        timeout(HttpRequest::TIMEOUT),
        verbose(HttpRequest::VERBOSE)
{
}

HttpRequest::HttpRequest(const HttpMethod& method,
                         const std::string& url,
                         const std::map<std::string, std::string>& headers,
                         InputStreamHandle content) :
        method(method),
        url(url),
        headers(headers),
        content(content),
        timeout(HttpRequest::TIMEOUT),
        verbose(HttpRequest::VERBOSE)
{
}

HttpRequest::HttpRequest(const HttpMethod& method,
                         const std::string& url,
                         const std::map<std::string, std::string>& headers,
                         InputStreamHandle content,
                         long timeout,
                         bool verbose) :
        method(method),
        url(url),
        headers(headers),
        content(content),
        timeout(timeout),
        verbose(verbose)
{
}

HttpRequest::HttpRequest(const Request& request)
{
    *this = convertRequest(request, HttpRequest::TIMEOUT, HttpRequest::VERBOSE);
}

HttpRequest::HttpRequest(const Request& request,
                         long timeout,
                         bool verbose) 
{
    *this = convertRequest(request, timeout, verbose);
}

bool HttpRequest::operator==(const HttpRequest& rhs) const
{
    return (method == rhs.method &&
            url == rhs.url &&
            headers == rhs.headers &&
            content == rhs.content &&
            timeout == rhs.timeout &&
            verbose == rhs.verbose);
}

bool HttpRequest::operator!=(const HttpRequest& rhs) const
{
    return !((*this) == rhs);
}

HttpMethod HttpRequest::getMethod() const
{
    return method;
}

void HttpRequest::setMethod(const HttpMethod& method)
{
    this->method = method;
}

HttpRequest& HttpRequest::withMethod(const HttpMethod& method)
{
    setMethod(method);
    return *this;
}

std::string HttpRequest::getUrl() const
{
    return url;
}

void HttpRequest::setUrl(const std::string& url)
{
    this->url = url;
}

HttpRequest& HttpRequest::withUrl(const std::string& url)
{
    setUrl(url);
    return *this;
}

std::map<std::string, std::string> HttpRequest::getHeaders() const
{
    return headers;
}

void HttpRequest::setHeaders(const std::map<std::string, std::string>& headers)
{
    this->headers = headers;
}

HttpRequest& HttpRequest::withHeaders(const std::map<std::string, std::string>& headers)
{
    setHeaders(headers);
    return *this;
}

InputStreamHandle HttpRequest::getContent() const
{
    return content;
}

void HttpRequest::setContent(InputStreamHandle content)
{
    this->content = content;
}

HttpRequest& HttpRequest::withContent(InputStreamHandle content)
{
    setContent(content);
    return *this;
}

long HttpRequest::getTimeout() const
{
    return timeout;
}

void HttpRequest::setTimeout(long timeout)
{
    this->timeout = timeout;
}

HttpRequest& HttpRequest::withTimeout(long timeout)
{
    setTimeout(timeout);
    return *this;
}

bool HttpRequest::getVerbose() const
{
    return verbose;
}

void HttpRequest::setVerbose(bool verbose)
{
    this->verbose = verbose;
}

HttpRequest& HttpRequest::withVerbose(bool verbose)
{
    setVerbose(verbose);
    return *this;
}

HttpRequest HttpRequest::convertRequest(
        const Request& request, long timeout, bool verbose)
{
    std::stringstream url;
    url << show(request.getEndpoint().getScheme());
    url << "://";
    url << request.getEndpoint().getHost();
    if (!request.getEndpoint().isUsingDefaultPort())
    {
        url << ':' << *(request.getEndpoint().getPort());
    }

    if (StringUtils::isEmpty(request.getResourcePath()))
    {
        url << "/";
    }
    else
    {
        url << HttpUtils::urlEncodePath(request.getResourcePath());
    }

    std::string queryString = HttpUtils::encodeParameters(request);
    InputStreamHandle content;

    if (request.getHttpMethod() != HttpMethod::POST || getContentSize(request) != 0)
    {
        if (queryString.size() != 0)
        {
            url << '?' << queryString;
        }

        if (request.getContent())
        {
            content = *(request.getContent());
        }
        else
        {
            content = StreamUtils::emptyInputStream();
        }
    }
    else
    {
        content = StreamUtils::setInput(queryString);
    }

    return HttpRequest(
            request.getHttpMethod(),
            url.str(),
            processHeaders(request.getHeaders()),
            content,
            timeout,
            verbose);
}

std::size_t HttpRequest::getContentSize(const Request& request)
{
    if (!request.getContent())
    {
        return 0;
    }
    else
    {
        return StreamUtils::getInputSize(*(request.getContent()));
    }
}

std::map<std::string, std::string> HttpRequest::processHeaders(
        const HeaderMap& headers)
{
    std::map<std::string, std::string> httpHeaders;
    std::transform(
            headers.begin(),
            headers.end(),
            std::inserter(httpHeaders, httpHeaders.end()),
            [] (const HeaderEntry& header) {
                std::string key = header.first;
                std::string value = (header.second) ? *(header.second) : "";
                return std::pair<const std::string, std::string>(key, value);
            });

    return httpHeaders;
}

}

