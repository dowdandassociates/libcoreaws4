/*
 *
 * libcoreaws4/src/coreaws4/Signer.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__SIGNER
#define COREAWS4__SIGNER

#include <ctime>
#include <functional>
#include <string>

#include "Buffer.hpp"
#include "Credentials.hpp"
#include "CredentialsProviderHandle.hpp"
#include "Endpoint.hpp"
#include "HeaderEntry.hpp"
#include "HeaderMap.hpp"
#include "ParameterEntry.hpp"
#include "ParameterMap.hpp"
#include "Request.hpp"

namespace coreaws4
{

class Signer
{
public:
    Signer() = default;
    Signer(const Signer&) = delete;
    Signer(Signer&&) = delete;
    virtual ~Signer();
    Signer& operator=(const Signer&) = delete;
    Signer& operator=(Signer&&) = delete;

    virtual Request operator()(const Credentials& credentials,
                               const Request& request,
                               std::time_t time) const = 0;
    virtual Request operator()(CredentialsProviderHandle credentials,
                               const Request& request,
                               std::time_t time) const;

protected:
    static std::string sign(const std::string& algorithm,
                            const Buffer& key,
                            const Buffer& data);
    static std::string sign(const std::string& algorithm,
                            const Buffer& key,
                            const std::string data);
    static std::string sign(const std::string& algorithm,
                            const std::string& key,
                            const Buffer& data);
    static std::string sign(const std::string& algorithm,
                            const std::string& key,
                            const std::string& data);

    static std::string canonicalizedEndpoint(const Endpoint& endpoint);
    static std::string canonicalizedQueryString(const ParameterMap& parameters);
    static std::string canonicalizedResourcePath(
            const std::string& resourcePath);

    static ParameterMap filterSignature(const ParameterMap& parameters);
    static HeaderMap lowerCaseKey(const HeaderMap& headers);
    static HeaderMap filterAuthorization(const HeaderMap& headers);
    static HeaderMap filterXAmznAuthorization(const HeaderMap& headers);

    static std::string constructQueryString(const ParameterMap& parameters,
            std::function<std::string (const std::string&, const ParameterEntry&)> accumulator);
};

}

#endif // not COREAWS4__SIGNER

