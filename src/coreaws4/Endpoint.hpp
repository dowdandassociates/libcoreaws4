/*
 *
 * libcoreaws4/src/coreaws4/Endpoint.hpp
 *
 *------------------------------------------------------------------------------
 * Copyright 2013 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS4__ENDPOINT
#define COREAWS4__ENDPOINT

#include <string>

#include <boost/optional.hpp>

#include "Port.hpp"
#include "Scheme.hpp"

namespace coreaws4
{

class Endpoint
{
public:
    Endpoint();
    Endpoint(const Endpoint& copy);
    Endpoint(Endpoint&& move);
    ~Endpoint();
    Endpoint& operator=(const Endpoint& rhs);
    Endpoint& operator=(Endpoint&& rhs);

    Endpoint(const Scheme& scheme,
             const std::string& host,
             const boost::optional<Port>& port);
    Endpoint(const Scheme& scheme,
             const std::string& host);
    Endpoint(const Scheme& scheme,
             const std::string& host,
             Port port);

    bool operator==(const Endpoint& rhs) const;
    bool operator!=(const Endpoint& rhs) const;

    std::string toString() const;
    bool isUsingDefaultPort() const;
    std::string getServer() const;

    Scheme getScheme() const;
    void setScheme(const Scheme& scheme);
    Endpoint& withScheme(const Scheme& scheme);

    std::string getHost() const;
    void setHost(const std::string& host);
    Endpoint& withHost(const std::string& host);

    boost::optional<Port> getPort() const;
    void setPort(boost::optional<Port> port);
    void setPort(Port port);
    Endpoint& withPort(const boost::optional<Port>& port);
    Endpoint& withPort(Port port);

private:
    Scheme scheme;
    std::string host;
    boost::optional<Port> port;
};

}

#endif // not COREAWS__ENDPOINT

