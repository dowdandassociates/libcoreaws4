## Requires

### Libraries

* openssl
* libcurl
* boost

## Build

```
export CXXFLAGS=-std=c++0x
autoreconf -i
./configure
make
```

## Run Samples

```
export AWS_ACCESS_KEY_ID=C99F5C7EE00F1EXAMPLE
export AWS_SECRET_KEY=a63xWEj9ZFbigxqA7wI3Nuwj3mte3RDBdEXAMPLE
cd src
./AWS2RequestTest
./AWS2RequestToCurl
./AWS3HttpsRequestTest
./AWS3HttpsRequestToCurl
./AWS3RequestTest
./AWS3RequestToCurl
./AWS4RequestTest
./AWS4RequestToCurl
./CloudFrontRequestTest
./CloudFrontRequestToCurl
./S3QueryStringRequestToCurl
./S3RequestTest
./S3RequestToCurl
```

